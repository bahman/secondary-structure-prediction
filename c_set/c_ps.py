import os
import math
import re
import sys
import traceback

def r_p_sec_two_col(p):
	""" This one reads the file with 2 columns,
		one column is the amino acid, the other is the secondary structure
	"""
	f = open(p, 'r')
	seq, sec = [], []
	for l in f:
		seq.append(l.split()[0])
		sec.append(l.split()[1][0])
	return ''.join(seq), ''.join(sec)

def r_p_sec(p):
	f = open(p, 'r')
	seq, sec = '', ''
	seqFound, secFound = False, False
	for l in f:
		if l.startswith('RES:'):
			seq = l.replace(',', '')[4:-1]
			seqFound = True
		if l.startswith('DSSP:'):
			sec = l.replace(',', '')[5:-1]
			secFound = True
		if seqFound and secFound:
			break
	return seq, sec


def c_sliding_win(seq, sec, win_len):
	in_seq, out_sec= [], []
	fin = len(seq)
	i = 0
	tmp_seq = ''
	while i < fin - win_len:
		if len(tmp_seq) == win_len:
			in_seq.append(tmp_seq)
			# calculate the sec struct of the current sequence
			out_sec.append(sec[int(i - win_len + math.ceil(win_len/2.0) - 1)])
			tmp_seq = ''
			i = i - win_len + 1
		else:
			tmp_seq += seq[i]
			i += 1
	return in_seq, out_sec
		

def r_amino_gr(amino_p):
	"""Constructs the pattern used in _find_freq and the 
		self.amino_classes from a given file
	"""
	try:
		amino_gr, tmp_amino_gr= {}, {}
		p = re.compile('[\r\n,\n]')
		f = open(amino_p, 'r')
		for l in f:
			l = p.sub ('', l)
			tmp_amino_gr[l[0]] = l
		cor_str = len(tmp_amino_gr) * '0'
		for i, (gr, aminos) in enumerate(tmp_amino_gr.items()):
			tmp_cor = [c for c in cor_str]
			tmp_cor[i] = '1'
			amino_gr[gr] = aminos, ''.join(tmp_cor)
		return amino_gr 
	except IOError:
		traceback.print_exception(sys.exc_info()[0], sys.exc_info()[1],
									  sys.exc_info()[2])

def replace_amino_gr(seq, groups):
	for group, (aminos, bins) in groups.items():
		for amino in aminos:
			seq = seq.replace(amino, group) 
	return seq


def replace_gr_bin(seq_bin, groups):
	for group, (aminos, bins) in groups.items():
		seq_bin = seq_bin.replace(group, bins)
	seq_bin = [str(i+1) + ':' + c for i, c in enumerate(seq_bin)]
	seq_bin = ' '.join(seq_bin)
	return seq_bin


def r_proteins(ps_p):
	""" Read proteins and return a long sequence of proteins concatenated 
		together

		Arguments:
		ps_p -- path to the directory which coniatns the proteins in seprate 
				files
	"""
	proteins_p = os.listdir(ps_p)
	proteins_p = map(lambda p: os.path.join(ps_p, p), proteins_p)
	seqs, secs = [], []
	for p in proteins_p:
		seq, sec = r_p_sec(p)
		seqs.append(seq)
		secs.append(sec)
	join_seq = ''.join(seqs)
	join_sec = ''.join(secs)
	return join_seq, join_sec, seqs, secs


def enc_sec(p_sec, struct_arr):
	# H | G                 = Helix
	# E                     = Strand
	# B | I | S | T | C | L = Other Structures
	all_sec = {'C': 1, 'B': 1, 'E': 1, 'G': 1, 'I': 1, 'H': 1, 'S': 1,
					'T': 1, '_': 1, '?': 1, 
					'5':1} # TODO WHAT IS 5
	p_sec = p_sec.upper()
	for sec, v in all_sec.items():
		for struct in struct_arr:
			if sec == struct:
				p_sec = p_sec.replace(sec, '1')
			else:
				p_sec = p_sec.replace(sec, '0')
	return p_sec


def w_libsvm(lib_p, in_arr, out_arr):
	with open(lib_p, 'w') as lib_f:
		for i, instance in enumerate(in_arr):
			if out_arr[i] == '0':
				out_arr[i] = '-1'
			lib_f.write(out_arr[i] + ' ' + instance + '\n')


def main(out_p, group_p, sec_struct):
	dir513 = os.path.abspath('../data/513_distribute')
	if sec_struct == 'H':
		sec = ['H', 'G'] # Helix
	elif sec_struct == 'B':
		sec = ['E'] # Sec Struct
	elif sec_struct == 'C':
		sec = ['B', 'I', 'S', 'T', 'C', 'L',
				'5'] # Other Structures # TODO WHAT IS 5
	else:
		raise Exception('No Secondary Structure was Sepcified')

	join_seq, join_sec, seqs, secs = r_proteins(dir513)
	#join_seq, join_sec = r_p_sec_two_col('../data/second_year_struct.txt')
	# There are 3 letters which are not in the groups for now they are removed
	aminos = 'IMVLFWYGPCASTNHQEDRK' # B X Z
	for i, c in enumerate(join_seq):
		if c not in aminos:
			join_seq = join_seq[:i]+'5'+join_seq[i+1:]
			join_sec = join_sec[:i]+'5'+join_sec[i+1:]
	for j, prot in enumerate(seqs):
		_prot = prot
		for i, c in enumerate(prot):
			if c not in aminos:
				seqs[j] = seqs[j][:i]+'5'+seqs[j][i+1:]
				secs[j] = seqs[j][:i]+'5'+seqs[j][i+1:]
	join_seq = join_seq.replace('5', '')
	seqs = [prot.replace('5', '') for prot in seqs]
	join_sec = join_sec.replace('5', '')
	secs = [prot.replace('5', '') for prot in secs]
	if join_seq[-1] not in aminos:
		join_seq = join_seq[:-1]
		join_sec = join_sec[:-1]
	#join_seq = join_seq[:30000]
	#join_sec = join_sec[:30000]
	##### end of remove operation
	join_sec = enc_sec(join_sec, sec)
	win_len = 13
	in_seq, out_sec = c_sliding_win(join_seq, join_sec, win_len)
	groups = r_amino_gr(group_p)
	gr_in_seq = map(lambda s: replace_amino_gr(s, groups), in_seq)
	bin_in_seq = map(lambda s: replace_gr_bin(s, groups), gr_in_seq)
	new_name =  group_p.split('/')[-1][:-4]
	w_libsvm(out_p+new_name+'.'+str(win_len)+'.'+sec_struct+'.libsvm', 
				bin_in_seq, out_sec)


if __name__ == '__main__':
	# python c_ps.py output_path path_to_alphabets [H | B | _ (For Sec Struct)]
	print  'python c_ps.py output_path path_to_alphabets [H | B | _ (For Sec Struct)]'
	if not os.path.exists (sys.argv[-2]):
		os.makedirs (sys.argv[-2])
	# argv[-3] output_path argv[-2] path_to_alphabets argv[-1] [Secondary structure to build]
	main (sys.argv[-2]+'/', sys.argv[-1], sys.argv[-3])
	print "FINISHED"
