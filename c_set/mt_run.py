'''
Created on 6 Aug 2011

@author: bahman
'''
#!/usr/bin/env python

import os
import sys
import traceback
import getpass
import pprint 
import random
from time import sleep
from threading import Thread
from subprocess import Popen, PIPE

from ssh_connection import SSHConnection
from ssh_connection import ConnThreadStopToken
from g_mr import SSHWorker, ssh_workers, WorkerStopToken, LocalWorker
if(sys.hexversion < 0x03000000):
	import Queue
else:
	import queue as Queue


ds_paths= []

# process command line options, set global parameters
def process_options(argv=sys.argv):

	global ds_paths
	usage = """
			Usage: mt_run.py -f alphabets
		"""
	if len(argv) < 2:
		print(usage)
		sys.exit(1)
	out_filename = "datasets_run.out"
	are_files = False
	i = 1
	while i < len(argv) :
		if are_files:
			assert os.path.exists(argv[i]),"dataset not found"
			ds_paths.append(argv[i])
		elif argv[i] == "-f":
			are_files = True
		i = i + 1
	if len(ds_paths) == 0:
		raise RuntimeError("The ds_paths cannot be empty.")


def calculate_jobs(out_dir, ds_paths):
	jobs = []
	for ds_p in ds_paths:
		for sec in ['H', 'B', 'C']:
			cmdline = 'python c_ps.py {0} {1} {2} '.format (sec, out_dir, ds_p)
			jobs.append(cmdline)	
	return jobs


def main():
	global ssh_workers, ds_paths
	# set parameters
	process_options()
	# put jobs in queue
	total_jobs = 0
	jobs = calculate_jobs('libsvm.all.13', ds_paths)
	job_queue = Queue.Queue(0)
	result_queue = Queue.Queue(0)
	print jobs, len(jobs)

	for cmdline in jobs:
		total_jobs += 1
		job_queue.put(cmdline)

	#Connect to hosts first
	hostnames = set(ssh_workers) #removing the extras
	hostnames = list(hostnames)
	#ssh_connector = SSHConnection(hostnames_l=hostnames)
	#ssh_connector.conn_to_clients_wait()
	#ssh_workers = [c.hostname for c in ssh_connector.con_clients_l] 
	#random.shuffle(ssh_workers)
	
	'''
	# fire ssh workers
	if ssh_workers:
		for host in ssh_workers:
			SSHWorker(host,job_queue,result_queue,host).start()
	'''

	# fire local workers
	nr_local_worker = 14
	for i in range(nr_local_worker):
		LocalWorker('local' + str(i), job_queue, result_queue).start()

	# gather results
	done_jobs = {}
	#result_f = open(out_filename, 'w')
	for cmdline in jobs:
		while cmdline not in done_jobs:
			(worker, suc_run) = result_queue.get()
			done_jobs[cmdline] = suc_run
			#result_f.write('{0} {1}\n'.format(cmdline, suc_run))
			#result_f.flush()
			nr_done_jobs = len(done_jobs)
			perc = nr_done_jobs * 100.0 / total_jobs
			perc = round(perc,3)
			state = (str(nr_done_jobs) + " out of " + str(total_jobs) + 
						" " + str(perc)+"%")
			result = '{0} {1}\n'.format(cmdline, suc_run)
			results = ''
			print(result + " " + state)

	job_queue.put(WorkerStopToken)
	print 'Finished all'
	#ssh_connector.close_clients()

if __name__ == '__main__':
	main()
