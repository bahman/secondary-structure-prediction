import matplotlib.pyplot as plt

all_g = [2,3,4,7,10,15,18]

ab = [78.8287, 68.5576, 75.0882, 84.4594, 83.3338, 69.0993, 59]
ab_g = all_g[:len(ab)]

dssp = [75.88, 77.6912, 78.6873, 82.4611, 83.657]
dssp_g = all_g[:len(dssp)]

gbmr = [54.5437,56.9202,71.9011,78.6611,84.3149]
gbmr_g = all_g[:len(gbmr)]

hsdm = [83.4542,60.5917,64.4226,78.6632,84,82.762]
hsdm_g = all_g[:len(hsdm)]

lwi = [83.4542,77.7243,79.3674,78.4315,73.3004,50,60]
lwi_g = all_g[:len(lwi)]

lwni = [83.4542,77.7213,78.4526,84.4315,79.1899,63.2761,60]
lwni_g = all_g[:len(lwni)]

lzbl = [83.081,50,78.8378,84.4202,83.7822,63.2761]
lzbl_g = all_g[:len(lzbl)]

lzmj = [83.4542,68.9669,75.3137,84.2327,82.735,69.7704]
lzmj_g = all_g[:len(lzmj)]

ml = [79.6112,50,78.4796,83.6528,84,64.9373]
ml_g = all_g[:len(ml)]

sdm = [83.4542,61.8285,66.9957,78.4345,84]
sdm_g = all_g[:len(sdm)]

td= [68.5576]
td_g = all_g[:len(td)]

lr= [84.9288]
lr_g = all_g[:len(lr)]

'''
plt.plot(
			ab_g, ab, '.', label='ab',
			#dssp_g, dssp, ',',
			#gbmr_g, gbmr, 'o',
			#hsdm_g, hsdm, 'v',
			lwi_g, lwi, '^',
			lwni_g, lwni, '<',
			#lzbl_g, lzbl, '>',
			#lzmj_g, lzmj, '*',
			#ml_g, ml, 'x',
			sdm_g, sdm, '+',
		)
'''
p_ab = plt.plot( ab_g, ab, '.', label='ab')
#p_dssp = plt.plot(dssp_g, dssp, ',', label='dssp')
p_lwi = plt.plot(lwi_g, lwi, '^', label='lwi')
p_lwni = plt.plot(lwni_g, lwni, '<', label='lwni')
#p_lzbl = plt.plot(lzbl_g, lzbl, '>', label='lzbl')
#p_lzmj = plt.plot(lzmj_g, lzmj, '*', label='lzmj')
#p_ml = plt.plot(ml_g, ml, 'x', label='ml')
p_sdm = plt.plot(sdm_g, sdm, '+', label='sdm')


p_arr = [
			p_ab,
			#p_dssp,
			p_lwi,
			p_lwni,
			#p_lzbl,
			#p_lzmj,
			#p_ml,
			p_sdm
		]

n_arr = [
			'ab',
			#'dssp',
			'lwi',
			'lwni',
			#'lzbl',
			#'lzmj',
			#'ml',
			'sdm'
		]

l1 = plt.legend(p_arr,n_arr,loc=1)

plt.axis([0,20,50,90])
plt.xlabel('Number In Group')
plt.ylabel('Accuracy %')
plt.show()
