'''
Created on 5 Aug 2011

@author: bahman
'''
import getpass
import sys
import traceback
import socket
from subprocess import Popen, PIPE
from threading import Thread, RLock, Condition
from time import sleep
if(sys.hexversion < 0x03000000):
	import Queue
else:
	import queue as Queue
	
import paramiko

# setup logging
#paramiko.util.log_to_file('demo_simple.log')

class ConnThreadStopToken:
	pass

class ConnThread(Thread):
	
	def __init__(self, username,password, host_q, clients_q, thread_done_q, cv):
		Thread.__init__(self)
		self.username = username
		self.password = password
		self.host_q = host_q
		self.clients_q = clients_q
		self.thread_done_q = thread_done_q
		self.cv = cv
		
	def run(self):
		while True:
			hostname = self.host_q.get()
			if hostname is ConnThreadStopToken:
				self.host_q.put(hostname) #For other threads to stop
				print 'Shutdown This Thread'
				self.cv.acquire()
				self.thread_done_q.put('Another One')
				self.cv.notify()
				self.cv.release()
				break
			port = 22
			# now, connect and use paramiko Client to negotiate SSH2 across 
			# the connection
			try:
				client = paramiko.SSHClient()
				client.load_system_host_keys()
				#print 'Connecting to ', hostname
				client.connect(hostname, port, self.username, self.password)
				client.hostname = hostname #Adding host name to client obj
				self.clients_q.put(client)
				#print client.exec_command('hostname')[1].readline()
			except socket.error, e:
				sys.stderr.write('Caught exception: %s: %s %s\n' % 
								(e.__class__, e, hostname))
			except paramiko.AuthenticationException, e:
				sys.stderr.write('Caught exception: %s: %s %s\n' % 
								(e.__class__, e, hostname))
			except Exception, e:
				self.host_q.put(hostname) #In case it does not connect 
				sys.stderr.write('Caught exception: %s: %s\n' % 
								(e.__class__, e))
				traceback.print_exc()
				try:
					client.close()
				except:
					pass

class SSHConnection(object):
	'''
	Class gets a l of hostnames to start with,
	'''
	def __init__(self, **kwargs):
		
		self.hostnames_l = kwargs.get('hostnames_l',None)
		self.username = kwargs.get('username', 'ba2g09')
		self.password = kwargs.get('password', None)
		self.hostnames_q = Queue.Queue(0)
		self.clients_q = Queue.Queue(0)
		self.thread_done_q = Queue.Queue(0)
		if self.password is None:
			self.password = getpass.getpass('Password:')
		self.cv = Condition()
		self.con_clients_l = []
		

	def conn_to_clients(self, **kwargs):
		hostnames_l = kwargs.get('hostnames_l', self.hostnames_l)
		for hostname in hostnames_l:
			self.hostnames_q.put(hostname)
		self.hostnames_q.put(ConnThreadStopToken)
		for i in range(7):
			ConnThread(self.username, self.password, self.hostnames_q,
					self.clients_q, self.thread_done_q, self.cv).start()

	def conn_to_clients_wait(self,**kwargs):
		self.conn_to_clients(**kwargs)
		notConn = True
		self.cv.acquire()
		while notConn:
			if self.thread_done_q.qsize() == 7:
				notConn = False
			else:
				self.cv.wait()
		self.cv.release()
		for c in range(self.clients_q.qsize()):
			self.con_clients_l.append(self.clients_q.get())
		for c in self.con_clients_l:
			self.clients_q.put(c)
			print 'Connected to', c.hostname
		print 'FINISHED CONNECTED TO', self.clients_q.qsize()


	def close_clients(self):
		print '-'*50
		print '-'*50
		print 'Close connection to clients'
		print '-'*50
		print '-'*50
		if self.clients_q.qsize() is 0:
			print 'No connection was successful'
		else:
			print self.clients_q.qsize(), ' Connected'
		while not self.clients_q.empty():
			c = self.clients_q.get()
			try:
				print 'Closing ' + c.hostname #Using the added hostname
				c.close()
			except Exception, e:
				sys.stderr.write('Caught exception: %s: %s' % 
								 (e.__class__, e))
				traceback.print_exc()
	

	def killall(self, proc_to_kill):
		for h in self.hostnames_l:
			cmdline = 'ssh ' + h + ' "killall ' + proc_to_kill +  '"'
			Popen(cmdline,shell=True,stdout=PIPE).stdout

		

hostnames_l =  (
		['zombie'] * 7 +
		['lich'] * 7 +
		['shark'] * 7 +
		['skeleton'] * 7 +
		['ghoul'] * 7 +
		['ghast'] * 7 +
		['whale'] * 7 +
		['porpoise'] * 7 +
		['seal'] * 7 +
		['banshee'] * 1 +
		['bass'] * 7 +
		['butterfish'] * 7 +
		['bream'] * 7 +
		['chard'] * 7 +
		['clam'] * 7 +
		['conch'] * 7 +
		['walrus'] * 7 +
		['manatee'] * 7 +
		['mummy'] * 7 +
		['wraith'] * 7 +
		['kelp'] * 7 +
		['octopus'] * 7 +
		['eel'] * 7 +
		['pumpkin'] * 7 +
		['potato'] * 7 +
		['cockle'] * 7 +
		['seacucumber'] * 7 +
		['seapineapple'] * 7 +
		['crab'] * 7 +
		['crayfish'] * 7 +
		['cuttlefish'] * 7 +
		['blowfish'] * 7 +
		['bluefish'] * 7 +
		['celery'] * 7 +
		['pignut'] * 7 +
		['parsnip'] * 7 +
		['swede'] * 7 +
		['sweetpepper'] * 7 +
		['sprout'] * 7 +
		['spinach'] * 7 +
		['brill'] * 7
		)


if __name__ == '__main__':
	# The following shows how this class could be used
	tmp_set = set(hostnames_l)
	a = SSHConnection(hostnames_l=tmp_set)
	a.conn_to_clients_wait()
	#a.killall('java')
	#a.killall('python')
	#a.killall('blastpgp')
	a.killall('svm-train')
