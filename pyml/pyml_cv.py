import sys
import pickle
import os
from multiprocessing import Pool

from PyML import *
from PyML import SparseDataSet



def train_test(ds_path):
	data = SparseDataSet(ds_path)
	g, c, fold = 0.15, 4, 5
	data.attachKernel('gaussian', gamma = g)
	s=SVM(C=c)
	r = s.cv(data, numFolds=fold)
	o = open(ds_path+'.pkl', 'wb')
	pickle.dump(r, o)
	o.close();
	print ds_path


if __name__ == '__main__':
	ds_ps = '/tmp/libsvm.H.13/'
	dss = os.listdir(ds_ps)
	dss = [ds_ps+p for p in dss]
	pool = Pool(22)
	pool.map(train_test, dss)
