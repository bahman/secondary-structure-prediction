import sys
import pickle
import os
from multiprocessing import Pool

from PyML import *
from PyML import SparseDataSet



def train_test(args):
	train_p = args[0]
	test_p = args[1]
	train = SparseDataSet(train_p)
	test = SparseDataSet(test_p)
	g, c, fold = 0.15, 4, 5
	train.attachKernel('gaussian', gamma = g)
	s=SVM(C=c)
	s.train(train)
	r = s.test(test)
	#r = s.cv(train, numFolds=fold)
	o = open(test_p+'.pkl', 'wb')
	pickle.dump(r, o)
	o.close();
	print train_p


if __name__ == '__main__':
	ds_ps = '/tmp/libsvm.H/'
	dss = os.listdir(ds_ps)
	dss = [p for p in dss if p.startswith('lz-bl')]
	dss = [ds_ps+p for p in dss]
	test_s = [p for p in dss if p.endswith('test.libsvm')]
	train_s = [p for p in dss if p.endswith('H.libsvm')]
	test_s.sort()
	train_s.sort()
	dss = []
	for i in range(len(train_s)):
		dss.append((train_s[i], test_s[i]))
	pool = Pool(14)
	pool.map(train_test, dss)
