import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon


def get_x_col(elem, *args):
	ret_arr = []
	for scheme in args:
		try:
			ret_arr.append(scheme[elem])		
		except IndexError:
			pass
			#ret_arr.append(0)
	return ret_arr

#Box Blot RBF 5k AUC
g11 = []
g10 = [0.78271559415160774, 0.75923592742791024, 0.7445162491110352, 0.7720032274524844, 0.75668354958651085, 0.7776111390697853, 0.75297739004705166, 0.76759972374770258, 0.75332951904094259, 0.78121629668113979]
g13 = []
g12 = []
g15 = [0.78346872484815266, 0.77706086393221652, 0.78467243135839326, 0.7734570089250935, 0.7831179485586558]
g14 = []
g17 = [0.7812161964808142, 0.77445695807342829, 0.7826682244477271, 0.78166466808767432]
g16 = []
g19 = [0.77565866057715893, 0.77926366788802515, 0.77726036278028854]
g18 = [0.77866196493339312, 0.78206787419748924, 0.77966226478286704]
g20 = [0.77580738291028328]
g3 = []
g2 = []
g5 = []
g4 = []
g7 = [0.77435745915020604, 0.7574841501872619, 0.73580538391378958, 0.7354559102285243, 0.7145750391470147, 0.77500738351148535]
g6 = []
g9 = []
g8 = []

# Only the last 5 are good for secondary structure
data = [g7, g10, g15, g16, g17, g18, g19, g20]
randomDists = ['10', '10', '15', '16','17','18','19','20']

# Generate some data from five different probability distributions,
# each with different characteristics. We want to play with how an IID
# bootstrap resample of the data preserves the distributional
# properties of the original sample, and a boxplot is one visual tool
# to make this assessment
'''
numDists = 9
randomDists = ['Normal(1,1)',' Lognormal(1,1)', 'Exp(1)', 'Gumbel(6,4)',
			 	'Normal(1,1)',' Lognormal(1,1)', 'Exp(1)', 'Gumbel(6,4)',
              'Triangular(2,9,11)']
'''
N = 500
norm = np.random.normal(1,1, N)
logn = np.random.lognormal(1,1, N)
expo = np.random.exponential(1, N)
gumb = np.random.gumbel(6, 4, N)
tria = np.random.triangular(2, 9, 11, N)

# Generate some random indices that we'll use to resample the original data
# arrays. For code brevity, just use the same random indices for each array
bootstrapIndices = np.random.random_integers(0, N-1, N)
normBoot = norm[bootstrapIndices]
expoBoot = expo[bootstrapIndices]
gumbBoot = gumb[bootstrapIndices]
lognBoot = logn[bootstrapIndices]
triaBoot = tria[bootstrapIndices]

'''
data = [norm, normBoot,  logn, lognBoot, expo, expoBoot, gumb, gumbBoot,
       tria]
'''

fig = plt.figure(figsize=(10,6))
fig.canvas.set_window_title('A Boxplot Example')
ax1 = fig.add_subplot(111)
plt.subplots_adjust(left=0.075, right=0.95, top=0.9, bottom=0.25)

bp = plt.boxplot(data, notch=0, sym='+', vert=1, whis=1.5)
#plt.setp(bp['boxes'], color='black')
#plt.setp(bp['whiskers'], color='black')
#plt.setp(bp['fliers'], color='red', marker='+')

'''
# Add a horizontal grid to the plot, but make it very light in color
# so we can use it for reading data values but not be distracting
ax1.yaxis.grid(True, linestyle='-', which='major', color='lightgrey',
              alpha=0.5)
'''

# Hide these grid behind plot objects
ax1.set_axisbelow(True)
#ax1.set_title('')
ax1.set_xlabel('Alphabet size')
ax1.set_ylabel('AUC')

# Now fill the boxes with desired colors
#boxColors = ['red','green','darkkhaki','royalblue']
'''
numBoxes = 9
medians = range(numBoxes)
for i in range(numBoxes):
  box = bp['boxes'][i]
  boxX = []
  boxY = []
  for j in range(5):
      boxX.append(box.get_xdata()[j])
      boxY.append(box.get_ydata()[j])
  boxCoords = zip(boxX,boxY)
  # Alternate between Dark Khaki and Royal Blue
  #k = i % 2
  #boxPolygon = Polygon(boxCoords, facecolor=boxColors[k])
  #ax1.add_patch(boxPolygon)
  # Now draw the median lines back over what we just filled in
  med = bp['medians'][i]
  medianX = []
  medianY = []
  for j in range(2):
      medianX.append(med.get_xdata()[j])
      medianY.append(med.get_ydata()[j])
      plt.plot(medianX, medianY, 'k')
      medians[i] = medianY[0]
  # Finally, overplot the sample averages, with horixzontal alignment
  # in the center of each box
  plt.plot([np.average(med.get_xdata())], [np.average(data[i])],
           color='w', marker='*', markeredgecolor='k')

'''
'''
# Set the axes ranges and axes labels
ax1.set_xlim(0.5, numBoxes+0.5)
top = 40
bottom = -5
ax1.set_ylim(bottom, top)
'''
#xtickNames = plt.setp(ax1, xticklabels=np.repeat(randomDists, 2))
xtickNames = plt.setp(ax1, xticklabels=randomDists)
#plt.setp(xtickNames, rotation=45, fontsize=8)
plt.setp(xtickNames)

'''
# Due to the Y-axis scale being different across samples, it can be
# hard to compare differences in medians across the samples. Add upper
# X-axis tick labels with the sample medians to aid in comparison
# (just use two decimal places of precision)
pos = np.arange(numBoxes)+1
upperLabels = [str(np.round(s, 2)) for s in medians]
weights = ['bold', 'semibold']
for tick,label in zip(range(numBoxes),ax1.get_xticklabels()):
   k = tick % 2
   ax1.text(pos[tick], top-(top*0.05), upperLabels[tick],
        horizontalalignment='center', size='x-small', weight=weights[k],
        color=boxColors[k])
'''

# Finally, add a basic legend
'''
plt.figtext(0.80, 0.08,  str(N) + ' Random Numbers' ,
           backgroundcolor=boxColors[0], color='black', weight='roman',
           size='x-small')
plt.figtext(0.80, 0.045, 'IID Bootstrap Resample',
backgroundcolor=boxColors[1],
           color='white', weight='roman', size='x-small')
plt.figtext(0.80, 0.015, '*', color='white', backgroundcolor='silver',
           weight='roman', size='medium')
plt.figtext(0.815, 0.013, ' Average Value', color='black', weight='roman',
           size='x-small')
'''
plt.grid(True)
plt.show()
