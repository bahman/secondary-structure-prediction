import sys


f = open (sys.argv[1], 'r')
b_data = [l.split() for l in f]
pos, neg = 0, 0
posSet, negSet = [], []
for p in b_data:
	if p[0] == '1':
		posSet.append(p)
		pos += 1
	else:
		negSet.append(p)
		neg += 1
print 'Pos', pos, 'Neg', neg, 'total', len(b_data)
print 'Percentage of Pos', pos*100/len(b_data)
