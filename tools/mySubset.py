import traceback
import sys
import random
import os



# process command line options, set global parameters
def process_options(argv=sys.argv):

	ds_paths = []
	usage = """
			Usage: mt_run.py -f datasets
		"""
	if len(argv) < 2:
		print(usage)
		sys.exit(1)
	out_filename = "datasets_run.out"
	are_files = False
	set_size = int(argv[1])
	i = 2
	while i < len(argv) :
		if are_files:
			assert os.path.exists(argv[i]),"dataset not found"
			ds_paths.append(argv[i])
		elif argv[i] == "-f":
			are_files = True
		i = i + 1
	if len(ds_paths) == 0:
		raise RuntimeError("The ds_paths cannot be empty.")
	return set_size, ds_paths


def cnt_num_instances(p):
	try:
		f = open(p, 'r')
		n = 0
		for l in f:
			n += 1
		return n
	except IOError:
		traceback.print_exception(sys.exc_info()[0], sys.exc_info()[1],
								  sys.exc_info()[2])
		

def func(*args):
	""" ds_path, out_path, indexes
	"""
	ds_path, out_path, rest_path, indexes = args[0]
	try:
		f = open(ds_path, 'r')
		arr = [l for l in f]
		rand_arr = []
		rest_arr = []
		for i, l in enumerate(arr):
			if i in indexes:
				rand_arr.append(l)
			else:
				rest_arr.append(l)
		f.close()
		out = open(out_path, 'w')
		for l in rand_arr:
			out.write(l)
		out.close()
		rest = open(rest_path, 'w')
		for l in rest_arr:
			rest.write(l)
		rest.close()
	except IOError:
		traceback.print_exception(sys.exc_info()[0], sys.exc_info()[1],
								  sys.exc_info()[2])

try:
	set_size, ds_paths = process_options()
	instance_num = cnt_num_instances(ds_paths[0])
	indexes = range(instance_num)
	random.shuffle(indexes)
	indexes = indexes[:set_size]

	args = []
	for path in ds_paths:
		prev = path.split('/')[-1].split('.')
		prev.insert(len(prev)-1, str(set_size))
		prev = '.'.join(prev)
		new = path.split('/')[-1].split('.')
		new.insert(len(new)-1, 'rest')
		new = '.'.join(new)
		args.append([path, '/tmp/tmp/'+prev,
					'/tmp/tmp/'+new, indexes])
	from multiprocessing import Pool
	p = Pool(14)
	p.map(func, args)
except:
	traceback.print_exception(sys.exc_info()[0], sys.exc_info()[1],
							  sys.exc_info()[2])
	print '''
		mySubset.py set_size -f your_data_set
	'''
