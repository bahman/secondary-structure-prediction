import pickle
import os
import pprint
from PyML import *


if __name__=='__main__':
	dir_set = '/tmp/libsvm.all.13/'
	fs = os.listdir(dir_set)
	fs.sort()
	fs = [dir_set+p for p in fs if p.endswith('.pkl')]
	rs = []
	names = {}
	for i in range(2,21):
		names[str(i)] = []
	for i, p in enumerate(fs):
		rs.append(pickle.load(open(p, 'rb')))
		#print fs[i].split('/')[-1], rs[i].getROC(), rs[i].getSuccessRate()
		n = fs[i].split('/')[-1]
		names[n.split('.')[1]].append(rs[i].getSuccessRate())
	for k,v in names.items():
		print 'g'+k, '=',  v
